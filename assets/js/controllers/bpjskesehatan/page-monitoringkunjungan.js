"use strict";

var baseUrl =
    window.location.protocol +
    "//" +
    window.location.host +
    "/" +
    window.location.pathname.split("/")[1];

function getProductsUrl() {
    return baseUrl + "/api/monitoringsep";
}

function getProducts() {
    return new Promise(function (resolve, reject) {
        const ajax = new XMLHttpRequest();
        ajax.onload = function () {

            try {
                const data = JSON.parse(ajax.responseText);
                if (ajax.status === 200) {
                    resolve(data);
                } else {
                    reject(data);
                }
                // resolve(JSON.parse(ajax.responseText));

            } catch (e) {
                reject(e);
            }
        };
        ajax.open("GET", getProductsUrl());
        ajax.send();
    })
}



function clearSeps() {
    const productUl = document.getElementById("seps");
    productUl.textContent = "";
}


function displaySep(seps) {

    let sepTr = document.createElement("tr");

    let sepTd1 = document.createElement("td");
    let sepTd2 = document.createElement("td");
    let sepTd3 = document.createElement("td");
    let sepTd4 = document.createElement("td");
    let sepTd5 = document.createElement("td");
    let sepTd6 = document.createElement("td");
    let sepTd7 = document.createElement("td");
    let sepTd8 = document.createElement("td");
    let sepTd9 = document.createElement("td");
    let sepTd10 = document.createElement("td");

    sepTd1.textContent = seps.noSep;
    sepTd2.textContent = seps.nama;
    sepTd3.textContent = seps.noKartu;
    sepTd4.textContent = seps.tglSep;
    sepTd5.textContent = seps.jnsPelayanan;
    sepTd6.textContent = seps.kelasRawat;
    sepTd7.textContent = seps.noRujukan;
    sepTd8.textContent = seps.poli;
    sepTd9.textContent = seps.tglPlgSep;
    sepTd10.textContent = seps.diagnosa;


    sepTr.appendChild(sepTd1);
    sepTr.appendChild(sepTd2);
    sepTr.appendChild(sepTd3);
    sepTr.appendChild(sepTd4);
    sepTr.appendChild(sepTd5);
    sepTr.appendChild(sepTd6);
    sepTr.appendChild(sepTd7);
    sepTr.appendChild(sepTd8);
    sepTr.appendChild(sepTd9);
    sepTr.appendChild(sepTd10);

    let sepsUl = document.getElementById("seps");
    sepsUl.appendChild(sepTr);
}


async function buttonClick() {
    try {
        console.log("start try.....");
        const value = await getProducts();
        console.log(value);
        console.log("flagg try.....");
        const seps = value.response.sep;
        clearSeps();




        seps.forEach(function (sep) {
            displaySep(sep);
        });
    } catch (error) {
        console.log("start catch");
        alert(error.message);
    } finally {
        console.log("Selesai memproses Async Await");
    }
}


$(function () {

    let list_monitoring_sep = $('#list_monitoring_sep').DataTable({
        'scrollX': true,
        'searchHighlight': true,
        'columnDefs': [
            { width: 5, targets: 0 },
            { width: 100, targets: 1 },
            { width: 250, targets: 4 }
        ],
        'fixedColumns': true
    });



});