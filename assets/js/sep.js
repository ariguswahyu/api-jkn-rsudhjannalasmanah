"use strict";

function PopupCenter(url, title, w, h) {
	// Fixes dual-screen position                         Most browsers      Firefox
	var dualScreenLeft =
		window.screenLeft != undefined ? window.screenLeft : window.screenX;
	var dualScreenTop =
		window.screenTop != undefined ? window.screenTop : window.screenY;

	var width = window.innerWidth
		? window.innerWidth
		: document.documentElement.clientWidth
			? document.documentElement.clientWidth
			: screen.width;
	var height = window.innerHeight
		? window.innerHeight
		: document.documentElement.clientHeight
			? document.documentElement.clientHeight
			: screen.height;

	var systemZoom = width / window.screen.availWidth;
	var left = (width - w) / 2 / systemZoom + dualScreenLeft;
	var top = (height - h) / 2 / systemZoom + dualScreenTop;
	var newWindow = window.open(
		url,
		title,
		"scrollbars=yes, width=" +
		w / systemZoom +
		", height=" +
		h / systemZoom +
		", top=" +
		top +
		", left=" +
		left
	);

	// Puts focus on the newWindow
	if (window.focus) newWindow.focus();
}

function selectedNoKunjungan(noKunjungan) {
	$("#tx_susulannorujukan_bpjs").val(noKunjungan);
}

$(function () {
	let baseUrl =
		window.location.protocol +
		"//" +
		window.location.host +
		"/" +
		window.location.pathname.split("/")[1];

	$(".sep-toggle").on("click", function () {
		$(".form-sep-advanced").toggle();
	});

	$(".reg-toggle-button").on("click", function () {
		//$(".reg-prof").toggle();

		let idx = $(this).data("idx");

		//alert(idx);
		PopupCenter(
			baseUrl + "/cetak/tandabuktipendaftaranrajal/" + idx + "",
			"xtf",
			"1200",
			"700"
		);
	});

	$(".sep-create").on("click", function () {
		let idx = $(this).data("idx");
		let type = $(this).data("type");
		if (idx) {
			// window.open(
			// 	baseUrl + "/rekammedis/SepCreate/" + type + "/" + idx + "",
			// 	"_blank"
			// );

			window.location.href = baseUrl + "/rekammedis/SepCreate/" + type + "/" + idx + "";

			// PopupCenter(
			// 	baseUrl + "/rekammedis/SepCreate/" + type + "/" + idx + " ",
			// 	"xtf",
			// 	"1200",
			// 	"700"
			// );
		}
	});

	$(".pengajuan-sep").on("click", function () {
		let jenislayanan = $(this).data("jenislayanan");
		let idx = $(this).data("idx");
		PopupCenter(
			baseUrl + "/rekammedis/pengajuanSEP/" + jenislayanan + "/" + idx + " ",
			"xtf",
			"1250",
			"900"
		);
	});
	$(".pengajuan-sep-ranap").on("click", function () {
		let jenislayanan = $(this).data("jenislayanan");
		let idx = $(this).data("idx");
		PopupCenter(
			baseUrl + "/rekammedis/pengajuanSEP/" + jenislayanan + "/" + idx + " ",
			"xtf",
			"1250",
			"900"
		);
	});

	$(".print-label").on("click", function () {
		let nomr = $(this).data("nomr");
		PopupCenter(
			baseUrl + "/cetak/labelidentitasPasien/" + nomr + "",
			"xtf",
			"900",
			"500"
		);
	});
	$(".print-gelang").on("click", function () {
		let nomr = $(this).data("nomr");
		PopupCenter(
			baseUrl + "/cetak/labelgelangPasien/" + nomr + "",
			"xtf",
			"900",
			"500"
		);
	});

	$(".print-sep").on("click", function () {
		let idx = $(this).data("idx");
		let sep = $(this).data("sep");

		PopupCenter(
			baseUrl + "/cetak/cetakSEP/2/" + sep + "/" + idx + "",
			"xtf",
			"1200",
			"700"
		);
	});

	$("#txtnmdiagnosa").autocomplete({
		source: baseUrl + "/rekammedis/searchdiagnosa",
		select: function (event, ui) {
			//console.log(ui);
			$('[name="txtnmdiagnosa"]').val(ui.item.label);
			$('[name="txtkddiagnosa"]').val(ui.item.description);
		}
	});

	$("#txtnmpoli").autocomplete({
		source: baseUrl + "/rekammedis/searchpoli",
		select: function (event, ui) {
			//console.log(ui);
			$('[name="txtnmpoli"]').val(ui.item.label);
			$('[name="txtkdpoli"]').val(ui.item.description);
		}
	});

	$("#txtnmdpjp").autocomplete({
		source: function (req, res) {
			let poli = $("#txtkdpoli").val();
			//console.log(req);
			$.ajax({
				url: baseUrl + "/rekammedis/searchDPJP/2/" + poli + "",
				type: "GET",
				minLength: 2,
				dataType: "json",
				data: {
					search: req.term
				},
				success: function (data) {
					//console.log(data);
					res(data);
				}
			});
		},
		select: function (event, ui) {
			$('[name="txtnmdpjp"]').val(ui.item.label);
			$('[name="txtkddpjp"]').val(ui.item.description);
		}
	});

	$(".cek-rujukan-faskes").on("click", function () {
		let noRujukan = $("#tx_susulannorujukan_bpjs").val();
		let type = $("#cb_asalrujukan").val();
		if (noRujukan) {
			let rujukan = {
				faskes: type,
				noRujukan: noRujukan
			};
			$.ajax({
				url: baseUrl + "/rekammedis/getRujukanByNorujukanBPJS",
				method: "POST",
				data: rujukan,
				dataType: "json",
				success: function (data) {
					if (data.metaData.code == 200) {
						Swal.fire({
							type: "success",
							title:
								data.response.rujukan.noKunjungan +
								"<br/>" +
								data.response.rujukan.peserta.nama +
								"<br/>" +
								data.response.rujukan.peserta.noKartu +
								"<br/>" +
								data.response.rujukan.provPerujuk.nama +
								"<br/> Poli tujuan : " +
								data.response.rujukan.poliRujukan.nama,
							footer: "<a href>RSUD AJIBARANG</a>"
						});
					} else {
						Swal.fire({
							type: "error",
							title: data.metaData.message,
							footer: "<a href>RSUD AJIBARANG</a>"
						});
					}
				}
			});
		} else {
			Swal.fire({
				type: "error",
				title: "Nomer Rujukan Belum Diisi",
				footer: "<a href>Periksa Kembali Nomer Kartu BPJS yang di masukan</a>"
			});
		}
	});
	$(".cek-kepesertaan-bpjs").on("click", function () {
		let noKa = $("#tx_susulannokepesertaan_bpjs").val();
		let type = $("#cb_asalrujukan").val();
		if (noKa) {
			let payload = {
				type: type,
				noKartu: noKa
			};

			//console.log(payload);

			$.ajax({
				url: baseUrl + "/rekammedis/cariNomerRujukanByNoka",
				method: "POST",
				data: payload,
				dataType: "json",
				success: function (data) {
					//console.log(data);

					if (data.metaData.code == 200) {
						let htmldata = "<h2>" + data.response.rujukan.noKunjungan + "</h2>";
						htmldata +=
							"<h2>Poli tujuan : " +
							data.response.rujukan.poliRujukan.nama +
							"</h2>";
						htmldata +=
							"<h6>" +
							data.response.rujukan.diagnosa.kode +
							"  - " +
							data.response.rujukan.diagnosa.nama +
							"</h6>";
						htmldata +=
							"<h2>" + data.response.rujukan.provPerujuk.nama + " </h2>";
						htmldata += "<h2>" + data.response.rujukan.tglKunjungan + " </h2>";

						Swal.fire({
							type: "success",
							title: data.metaData.message,
							html: htmldata,
							footer: "<a href>RSUD AJIBARANG</a>"
						});
						$("#tx_susulannorujukan_bpjs").val(
							data.response.rujukan.noKunjungan
						);
					} else {
						Swal.fire({
							type: "error",
							title: data.metaData.message,
							footer: "<a href>RSUD AJIBARANG</a>"
						});
					}
				}
			});
		} else {
			Swal.fire({
				type: "error",
				title: "Nomer Kartu Belum Diisi",
				footer: "<a href>Periksa Kembali Nomer Kartu BPJS yang di masukan</a>"
			});
		}
	});

	$(".form-pulangkan-pasien-bpjs").on("click", function () {
		let idx = $(this).data("idx");
		let noKa = $(this).data("noka");//"0002481534551";
		//alert(noKa);
		window.location.href = baseUrl + "/rekammedis/formPasienPulangbpjs/" + idx + "/" + noKa + "";

	});

	$(".btn_back_konfirmasi_pendaftaran").on("click", function () {

		let idx = $(this).data("idx");
		let type = $(this).data("type");
		window.location.href = baseUrl + "/rekammedis/konfirmasipendaftaran/" + type + "/" + idx + "";

	});

	$(".btnListRujukan").on("click", function () {
		let noKa = $("#tx_susulannokepesertaan_bpjs").val();

		if (noKa) {
			let payload = {
				noKartu: noKa
			};
			$.ajax({
				url: baseUrl + "/rekammedis/lisRujukanBPJS",
				method: "POST",
				data: payload,
				dataType: "json",
				success: function (data) {
					//console.log(data);
					$("#table_fakes1 tbody").html("");
					$("#table_fakes2 tbody").html("");
					$("#table_last_rujukan_pcare tbody").html("");
					$("#table_last_rujukan_rs tbody").html("");

					let message = "";
					if (data.pcare.response) {
						$("#table_fakes1 tbody").html("");
						let response = data.pcare.response.rujukan;

						$.each(response, function (d, results) {
							$("#table_fakes1 tbody").append(
								"<tr>" +
								'<td><button type="button" onclick="selectedNoKunjungan(\'' +
								results.noKunjungan +
								'\')" class="btn btn-block btn-success btn-xs"   data-dismiss="modal">' +
								results.noKunjungan +
								"</button></td>" +
								"<td>" +
								results.peserta.nama +
								"  (" +
								results.peserta.noKartu +
								")</td>" +
								"<td>" +
								results.diagnosa.nama +
								"</td>" +
								"<td>" +
								results.pelayanan.nama +
								"</td>" +
								"<td>" +
								results.poliRujukan.nama +
								"</td>" +
								"<td>" +
								results.provPerujuk.nama +
								"</td>" +
								"<td>" +
								results.tglKunjungan +
								"</td>" +
								"</tr>"
							);
						});
					} else {
						$("#message-faskes-1").html("");
						message =
							'<div class="alert alert-danger alert-dismissible"><a href="#"  data-dismiss="alert" ></a><strong>' +
							data.pcare.metaData.message +
							"</strong></div>";
						$("#message-faskes-1").append(message);
					}

					if (data.rs.response) {
						$("#table_fakes2 tbody").html("");
						let response = data.rs.response.rujukan;
						$.each(response, function (d, results) {
							$("#table_fakes2 tbody").append(
								"<tr>" +
								// "<td>" +
								// results.noKunjungan +
								// "</td>" +

								'<td><button type="button" onclick="selectedNoKunjungan(\'' +
								results.noKunjungan +
								'\')" class="btn btn-block btn-success btn-xs"   data-dismiss="modal">' +
								results.noKunjungan +
								"</button></td>" +
								"<td>" +
								results.peserta.nama +
								"  (" +
								results.peserta.noKartu +
								")</td>" +
								"<td>" +
								results.diagnosa.nama +
								"</td>" +
								"<td>" +
								results.pelayanan.nama +
								"</td>" +
								"<td>" +
								results.poliRujukan.nama +
								"</td>" +
								"<td>" +
								results.provPerujuk.nama +
								"</td>" +
								"<td>" +
								results.tglKunjungan +
								"</td>" +
								"</tr>"
							);
						});
					} else {
						$("#message-faskes-2").html("");
						message =
							'<div class="alert alert-danger alert-dismissible"><a href="#"  data-dismiss="alert" ></a><strong>' +
							data.rs.metaData.message +
							"</strong></div>";
						$("#message-faskes-2").append(message);
					}

					if (data.peserta.response) {
						$("#dialog_lblnama").text(data.peserta.response.peserta.nama);
						$("#dialog_lblnoka").text(
							data.peserta.response.peserta.noKartu +
							"(" +
							data.peserta.response.peserta.mr.noMR +
							")"
						);

						$("#status-peserta").html("");
						let statuscode = data.peserta.response.peserta.statusPeserta.kode;
						//console.log("status " + statuscode);
						let mark = " alert alert-warning alert-dismissible";
						if (statuscode == "0") {
							mark = " alert alert-success alert-dismissible";
						} else {
							mark = " alert alert-danger alert-dismissible";
						}
						message =
							'<div class="' +
							mark +
							'"><a href="#"  data-dismiss="alert" ></a><strong> Status Kepesertaan BPJS : ' +
							data.peserta.response.peserta.statusPeserta.keterangan +
							"</strong></div>";
						$("#status-peserta").append(message);
					} else {
						$("#message-peserta").html("");
						message =
							'<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' +
							data.peserta.metaData.message +
							"</strong></div>";
						$("#message-peserta").append(message);
					}

					if (data.lastpcare.response) {
						$("#table_last_rujukan_pcare tbody").html("");
						if (data.lastpcare.metaData.code == 200) {
							let response = data.lastpcare.response.rujukan;
							$("#table_last_rujukan_pcare tbody").append(
								"<tr>" +
								// "<td>" +
								// response.noKunjungan +
								// "</td>" +

								'<td><button type="button" onclick="selectedNoKunjungan(\'' +
								response.noKunjungan +
								'\')" class="btn btn-block btn-success btn-xs"   data-dismiss="modal">' +
								response.noKunjungan +
								"</button></td>" +
								"<td>" +
								response.peserta.nama +
								"  (" +
								response.peserta.noKartu +
								")</td>" +
								"<td>" +
								response.diagnosa.nama +
								"</td>" +
								"<td>" +
								response.pelayanan.nama +
								"</td>" +
								"<td>" +
								response.poliRujukan.nama +
								"</td>" +
								"<td>" +
								response.provPerujuk.nama +
								"</td>" +
								"<td>" +
								response.tglKunjungan +
								"</td>" +
								"</tr>"
							);
						}
					} else {
						$("#message_table_last_rujukan_pcare").html("");
						message =
							'<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' +
							data.lastpcare.metaData.message +
							"</strong></div>";
						$("#message_table_last_rujukan_pcare").append(message);
					}

					if (data.lastrs.response) {
						$("#table_last_rujukan_rs tbody").html("");
						if (data.lastrs.metaData.code == 200) {
							let response = data.lastrs.response.rujukan;

							$("#table_last_rujukan_rs tbody").append(
								"<tr>" +
								// "<td>" +
								// response.noKunjungan +
								// "</td>" +

								'<td><button type="button" onclick="selectedNoKunjungan(\'' +
								response.noKunjungan +
								'\')" class="btn btn-block btn-success btn-xs"   data-dismiss="modal">' +
								response.noKunjungan +
								"</button></td>" +
								"<td>" +
								response.peserta.nama +
								"  (" +
								response.peserta.noKartu +
								")</td>" +
								"<td>" +
								response.diagnosa.nama +
								"</td>" +
								"<td>" +
								response.pelayanan.nama +
								"</td>" +
								"<td>" +
								response.poliRujukan.nama +
								"</td>" +
								"<td>" +
								response.provPerujuk.nama +
								"</td>" +
								"<td>" +
								response.tglKunjungan +
								"</td>" +
								"</tr>"
							);
						}
					} else {
						$("#message_table_last_rujukan_rs").html("");
						message =
							'<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' +
							data.lastrs.metaData.message +
							"</strong></div>";
						$("#message_table_last_rujukan_rs").append(message);
					}

					$("#modal-default").modal({
						show: "false"
					});
				}
			});
		} else {
			Swal.fire({
				type: "error",
				title: "Nomer KArtu Belum Diisi",
				footer: "<a href>Periksa Kembali Nomer Kartu BPJS yang di masukan</a>"
			});
		}
	});


});






