"use strict";

$(function() {
	$(".tanggal").datepicker({
		format: "dd-mm-yyyy"
	});

	$(".input-access-status-bayar-kasir").change(function() {
		//console.log("radio clicked!");
		let id = $(this).val();
		const idx = $(this).data("idx");

		let data = {
			id: id,
			idx: idx
		};

		let baseUrl =
			window.location.protocol +
			"//" +
			window.location.host +
			"/" +
			window.location.pathname.split("/")[1];

		console.log(data);
		$.ajax({
			type: "POST",
			url: baseUrl + "/kasir/ubahstatusbayarkasir",
			data: data,
			success: function(data, textStatus, jqXHR) {
				document.location.href = window.location;
				//console.log("dad");
				//console.log(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus);
				console.log(jqXHR);
				console.log("SEP LOKAl AJAX call failed.");
			}
		});
	});

	$(".flag-tagihan-kasir").on("click", function() {
		let tagihan = $(this).data("tagihan");
		let flag = $(this).data("flag");
		let idxdaftar = $(this).data("idxdaftar");
		let nomr = $(this).data("nomr");
		let jenislayanan = $(this).data("jenislayanan");
		let data = {
			idxdaftar: idxdaftar,
			nomr: nomr,
			flag: flag,
			jenislayanan: jenislayanan,
			tagihan: tagihan
		};
		//console.log(data);

		let baseUrl =
			window.location.protocol +
			"//" +
			window.location.host +
			"/" +
			window.location.pathname.split("/")[1];

		$.ajax({
			type: "POST",
			url: baseUrl + "/kasir/lunasbayar",
			data: data,
			success: function(data, textStatus, jqXHR) {
				// console.log(data);
				document.location.href = window.location;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(textStatus);
				console.log(jqXHR);
				console.log("SEP LOKAl AJAX call failed.");
			}
		});
	});

	$(".hapus_pelayanan_tmno").on("click", function() {
		let idx = $(this).data("idx");
		let idtmno = $(this).data("idtmno");
		let id = $(this).data("id");

		let data = {
			idx: idx,
			idtmno: idtmno,
			id: id
		};

		// console.log(data);
		let baseUrl =
			window.location.protocol +
			"//" +
			window.location.host +
			"/" +
			window.location.pathname.split("/")[1];

		$.ajax({
			url: baseUrl + "/kasir/hapus_y_tindakan_diagnosa_tindakan",
			method: "POST",
			data: data,
			success: function(data, textStatus, jqXHR) {
				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log("SEP LOKAl AJAX call failed.");
			}
		});
	});

	$(".add_cart").on("click", function() {
		let idx = $(this).data("idx");
		let idtmno = $(this).data("idtmno");
		let keterangan = $(this).data("keterangan");

		let data = {
			idx: idx,
			idtmno: idtmno,
			keterangan: keterangan
		};
		let baseUrl =
			window.location.protocol +
			"//" +
			window.location.host +
			"/" +
			window.location.pathname.split("/")[1];

		console.log(data);
		$.ajax({
			url: baseUrl + "/kasir/tambahcart",
			method: "POST",
			data: data,
			success: function(data, textStatus, jqXHR) {
				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log("SEP LOKAl AJAX call failed.");
			}
		});
	});

	$(".hapus_cart").on("click", function() {
		let idx = $(this).data("idx");
		let idtmno = $(this).data("idtmno");
		let idxbayar = $(this).data("idxbayar");

		let baseUrl =
			window.location.protocol +
			"//" +
			window.location.host +
			"/" +
			window.location.pathname.split("/")[1];

		$.ajax({
			url: baseUrl + "/kasir/hapuscart",
			method: "POST",
			data: {
				idxbayar: idxbayar,
				idtmno: idtmno
			},
			success: function(data, textStatus, jqXHR) {
				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log("SEP LOKAl AJAX call failed.");
			}
		});
	});

	$(".simpanBilling").on("click", function() {
		let nomr = $(this).data("nomr");
		let shift = $(this).data("shift");
		let nip = $(this).data("nip");
		let idxdaftar = $(this).data("idxdaftar");
		let tanggal = $(this).data("tanggal");
		let lunas = $(this).data("lunas");
		let jambayar = $(this).data("jambayar");
		let ip = $(this).data("ip");
		let kdcarabayar = $(this).data("kdcarabayar");
		let poli = $(this).data("poli");
		let aps = $(this).data("aps");
		let unit = $(this).data("unit");

		let data = {
			nomr: nomr,
			shift: shift,
			nip: nip,
			idxdaftar: idxdaftar,
			tanggal: tanggal,
			lunas: lunas,
			jambayar: jambayar,
			ip: ip,
			kdcarabayar: kdcarabayar,
			poli: poli,
			aps: aps,
			unit: unit
		};

		//console.log(data);

		let baseUrl =
			window.location.protocol +
			"//" +
			window.location.host +
			"/" +
			window.location.pathname.split("/")[1];

		$.ajax({
			url: baseUrl + "/kasir/simpanCartToBilling",
			method: "POST",
			data: data,
			success: function(data, textStatus, jqXHR) {
				//
				let obj = $.parseJSON(data);
				console.log(obj);

				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log("AJAX call failed.");
			}
		});
	});

	$(".hapus_marking_pembayaran").on("click", function() {
		//data-tagihan="' . $tagihan . '" data-idxdaftar="' . $idxdaftar . '" data-nomr="' . $nomr . '" data-jenislayanan="2" data-flag="' . $flag . '"

		let tagihan = $(this).data("tagihan");
		let idxdaftar = $(this).data("idxdaftar");
		let nomr = $(this).data("nomr");
		let jenislayanan = $(this).data("jenislayanan");
		let flag = $(this).data("flag");

		let data = {
			tagihan: tagihan,
			idxdaftar: idxdaftar,
			nomr: nomr,
			jenislayanan: jenislayanan,
			flag: flag
		};

		//console.log(data);

		let baseUrl =
			window.location.protocol +
			"//" +
			window.location.host +
			"/" +
			window.location.pathname.split("/")[1];

		$.ajax({
			url: baseUrl + "/kasir/hapusmarkingPayment",
			method: "POST",
			data: data,
			success: function(data, textStatus, jqXHR) {
				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log("SEP LOKAl AJAX call failed.");
			}
		});
	});
});
