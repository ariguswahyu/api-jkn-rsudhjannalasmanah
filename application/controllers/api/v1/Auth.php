<?php

defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Auth extends BD_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Users_model", "users");
    }

    function index_get()
    {
        echo "index";
    }


    public function login_post()
    {
        $username       = clean_data(htmlspecialchars($this->post('username'), true));
        $password       = clean_data(htmlspecialchars($this->input->post('password'), true));

        $key = $this->config->item('thekey');
        $findUser = $this->users->getLogin($username, $password);

        if ($findUser->num_rows() == 0) {
            $result["metaData"]["code"] = REST_Controller::HTTP_OK;
            $result["metaData"]["message"] = "User Tidak DItemukan";
            $result["response"] = null;
            $this->response($result,  $result["metaData"]["code"]);
        } else {
            $date = new DateTime();
            $token['iat'] = $date->getTimestamp();
            $token['exp'] =  strtotime("+1 week");
            $token['iss'] =  "RSUD HJ ANNA LASMANAH BANJARNEGARA";
            $output['token'] = JWT::encode($token, $key);
            $result["metaData"]["code"] = REST_Controller::HTTP_OK;
            $result["response"] = [
                "token" =>  $output['token']
            ];

            $this->response($result,  $result["metaData"]["code"]);
        }
    }
}
