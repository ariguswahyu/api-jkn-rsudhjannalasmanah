<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Poliklinik extends BD_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->load->model('Poli_model', 'poli');
    }

    public function index_get($kodepoli = null)
    {
        $poliklinik = $this->poli->getAllPoliklinik($kodepoli);
        $this->response($poliklinik,  200);
    }
}
