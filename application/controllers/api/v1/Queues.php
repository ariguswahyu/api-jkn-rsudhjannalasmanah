<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Queues extends BD_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->load->model('Poli_model', 'poli');
    }

    public function index_get()
    {
        //
    }
    public function getNoAntrian_post()
    {

        $nomorkartu         = clean_data(htmlspecialchars($this->post('nomorkartu'), true));
        $nik                = clean_data(htmlspecialchars($this->post('nik'), true));
        $notelp             = clean_data(htmlspecialchars($this->post('notelp'), true));
        $tanggalperiksa     = clean_data(htmlspecialchars($this->post('tanggalperiksa'), true));
        $kodepoli           = clean_data(htmlspecialchars($this->post('kodepoli'), true));
        $nomorreferensi     = clean_data(htmlspecialchars($this->post('nomorreferensi'), true));
        $jenisreferensi     = clean_data(htmlspecialchars($this->post('jenisreferensi'), true));
        $jenisrequest       = clean_data(htmlspecialchars($this->post('jenisrequest'), true));
        $polieksekutif      = clean_data(htmlspecialchars($this->post('polieksekutif'), true));

        $date = new DateTime;
        if ($nomorkartu && $nik  && $notelp && $tanggalperiksa && $kodepoli && $nomorreferensi && $jenisreferensi && $jenisrequest  && $polieksekutif) {
            $result["metaData"]["code"] = REST_Controller::HTTP_OK;
            $result["metaData"]["message"] = "Ok";
            $result["response"]["nomorantrean"] = "A10";
            $result["response"]["kodebooking"] = "QWERTYUIO123";
            $result["response"]["jenisantrean"] = "2";
            $result["response"]["estimasidilayani"] = $date->getTimestamp();
            $result["response"]["namapoli"] = "Poli Jantung";
            $result["response"]["namapoli"] = "dr. Poli Jantung S.Pj";
        } else {
            $result["metaData"]["code"] = REST_Controller::HTTP_NOT_ACCEPTABLE;
            $result["metaData"]["message"] = "Not Acceptable";
            $result["metaData"]["description"] = "Format Parameter Vaue Tidak Sesuai";
            $result["response"]["pesan"] = "Parameter Kosong";
        }
        $this->response($result,  $result["metaData"]["code"]);
        // - NOMOR KARTU
        // - NIK
        // - NO TELP
        // - TANGGAL PERIKSA (YYYY-MM-DD)
        // - KODE POLI (SESUAI REFERENSI POLI BPJS)
        // - NOMOR REFERENSI (NOMOR RUJUKAN / NOMOR KONTROL)
        // - JENIS RFERENSI (NOMOR RUJUKAN [1] / NOMOR KONTROL [2])
        // - JENIS REQUEST (PENDAFTARAN [1] / POLI [2])
        // - POLI EKSEKUTIF (1=Poli Eksekutif, 0=Poli Reguler)
    }


    public function getRekapAntrian_post()
    {

        $tanggalperiksa         = clean_data(htmlspecialchars($this->post('tanggalperiksa'), true));
        $kodepoli                = clean_data(htmlspecialchars($this->post('kodepoli'), true));
        $polieksekutif             = clean_data(htmlspecialchars($this->post('polieksekutif'), true));
        // - TANGGAL PERIKSA (YYYY-MM-DD)
        // - KODE POLI
        // - POLI EKSEKUTIF (1=Poli Eksekutif, 0=Poli Biasa)
        $date = new DateTime;
        if ($tanggalperiksa && $kodepoli && $polieksekutif) {
            $result["metaData"]["code"] = REST_Controller::HTTP_OK;
            $result["metaData"]["message"] = "Ok";

            $result["response"]["namapoli"] = "Poli Jantung";
            $result["response"]["totalantrean"] = 100;
            $result["response"]["jumlahterlayani"] = 20;
            $result["response"]["lastupdate"] = $date->getTimestamp();
        } else {
            $result["metaData"]["code"] = REST_Controller::HTTP_NOT_ACCEPTABLE;
            $result["metaData"]["message"] = "Not Acceptable";
            $result["metaData"]["description"] = "Format Parameter Vaue Tidak Sesuai";
            $result["response"]["pesan"] = "Parameter Kosong";
        }
        $this->response($result,  $result["metaData"]["code"]);
        // - NOMOR KARTU
        // - NIK
        // - NO TELP
        // - TANGGAL PERIKSA (YYYY-MM-DD)
        // - KODE POLI (SESUAI REFERENSI POLI BPJS)
        // - NOMOR REFERENSI (NOMOR RUJUKAN / NOMOR KONTROL)
        // - JENIS RFERENSI (NOMOR RUJUKAN [1] / NOMOR KONTROL [2])
        // - JENIS REQUEST (PENDAFTARAN [1] / POLI [2])
        // - POLI EKSEKUTIF (1=Poli Eksekutif, 0=Poli Reguler)
    }
}
