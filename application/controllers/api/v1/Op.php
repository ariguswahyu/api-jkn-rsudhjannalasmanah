<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Op extends BD_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->load->model('Poli_model', 'poli');
    }

    public function index_get()
    {
        // $poliklinik = $this->poli->getAllPoliklinik($kodepoli);
        // $this->response($poliklinik,  200);
    }
    public function getBookingOp_post()
    {

        $nopeserta         = clean_data(htmlspecialchars($this->post('nopeserta'), true));
        if ($nopeserta) {
            $result["metaData"]["code"] = REST_Controller::HTTP_OK;
            $result["metaData"]["message"] = "Ok";

            $listOp = array();
            $a = array("kodebooking" => "123456ZXC", "tanggaloperasi" => "2019-12-11", "jenistindakan" => "001", "kodepoli" => "001", "namapoli" => "Poli Bedah Mulut", "terlaksana" => 0);
            $b = array("kodebooking" => "78778787fedfe", "tanggaloperasi" => "2019-12-11", "jenistindakan" => "001", "kodepoli" => "001", "namapoli" => "Poli Kandungan", "terlaksana" => 0);
            $c = array("kodebooking" => "565665gtrde", "tanggaloperasi" => "2019-12-11", "jenistindakan" => "001", "kodepoli" => "001", "namapoli" => "Poli Mata", "terlaksana" => 0);
            $d = array("kodebooking" => "56565rer", "tanggaloperasi" => "2019-12-11", "jenistindakan" => "001", "kodepoli" => "001", "namapoli" => "Poli Kandungan", "terlaksana" => 0);

            array_push($listOp, $a);
            array_push($listOp, $b);
            array_push($listOp, $c);
            array_push($listOp, $d);

            $result["response"]["list"] = $listOp;
        } else {
            $result["metaData"]["code"] = REST_Controller::HTTP_NOT_ACCEPTABLE;
            $result["metaData"]["message"] = "Not Acceptable";
            $result["metaData"]["description"] = "Format Parameter Vaue Tidak Sesuai";
            $result["response"]["pesan"] = "Parameter Kosong";
        }
        $this->response($result,  $result["metaData"]["code"]);

        //  "kodebooking": "123456ZXC",
        //  "tanggaloperasi": "2019-12-11",
        //  "jenistindakan": "operasi gigi",
        //  "kodepoli": "001",
        //  "namapoli": "Poli Bedah Mulut",
        //  "terlaksana": 0 


        // $result["response"]["totalantrean"] = 100;
        // $result["response"]["jumlahterlayani"] = 20;
        // $result["response"]["lastupdate"] = $date->getTimestamp();
    }

    public function jadwalOp_post()
    {
        $tanggalawal         = clean_data(htmlspecialchars($this->post('tanggalawal'), true));
        $tanggalakhir         = clean_data(htmlspecialchars($this->post('tanggalakhir'), true));


        $date = new DateTime;
        if ($tanggalakhir && $tanggalawal) {

            $result["metaData"]["code"] = REST_Controller::HTTP_OK;
            $result["metaData"]["message"] = "Ok";

            $listOp = array();
            $a = array(
                "kodebooking" => "123456ZXC",
                "tanggaloperasi" => "2019-12-11",
                "jenistindakan" => "001",
                "kodepoli" => "001",
                "namapoli" => "Poli Bedah Mulut",
                "terlaksana" => 0,
                "nopeserta" => "nopeserta",
                "lastupdate" =>  $date->getTimestamp()
            );
            $b = array(
                "kodebooking" => "78788787",
                "tanggaloperasi" => "2019-12-11",
                "jenistindakan" => "001",
                "kodepoli" => "001",
                "namapoli" => "Poli Bedah Mulut",
                "terlaksana" => 0,
                "nopeserta" => "nopeserta",
                "lastupdate" =>  $date->getTimestamp()
            );
            $c = array(
                "kodebooking" => "gyggygyg",
                "tanggaloperasi" => "2019-12-11",
                "jenistindakan" => "001",
                "kodepoli" => "001",
                "namapoli" => "Poli Bedah Mulut",
                "terlaksana" => 0,
                "nopeserta" => "nopeserta",
                "lastupdate" =>  $date->getTimestamp()
            );

            array_push($listOp, $a);
            array_push($listOp, $b);
            array_push($listOp, $c);

            $result["response"]["list"] = $listOp;
        } else {
            $result["metaData"]["code"] = REST_Controller::HTTP_NOT_ACCEPTABLE;
            $result["metaData"]["message"] = "Not Acceptable";
            $result["metaData"]["description"] = "Format Parameter Vaue Tidak Sesuai";
            $result["response"]["pesan"] = "Parameter Kosong";
        }
        $this->response($result,  $result["metaData"]["code"]);
    }
}
