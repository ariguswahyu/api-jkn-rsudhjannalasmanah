<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Registration extends BD_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Registration_model', 'registration');
        $this->load->model('Pasien_model', 'pasien');
        $this->load->model('User_model', 'user');
        $this->auth();
        date_default_timezone_set('Asia/Jakarta');
    }


    public function outPatient_post()
    {
        $data = $this->input->post();
        if ($data["pasienbaru"] > 1) {
            $response["metaData"]["code"] = REST_Controller::HTTP_METHOD_NOT_ALLOWED;
            $response["metaData"]["message"] = "FORMAT SALAH";
            $response["response"] = null;
            $this->response($response,  $response["metaData"]["code"]);
        } else {
            $response = $this->registration->saveRegistration($data);
            if ($response["metaData"]["code"] == 201) {
                $user = $this->user->getDetailByID($data["userid"]);
                $this->sendEmail($user["email"]);
            }
            $this->response($response,  $response["metaData"]["code"]);
        }
    }

    public function outPatientList_get()
    {
        $config = [
            [
                'field' => 'id',
                'label' => 'KODE USER ID',
                'rules' => 'required|min_length[1]',
                'errors' => [
                    'required' => 'ID KOSONG',
                    'min_length' => 'Minimum Karakter 1'
                ],
            ]
        ];


        $data = $this->input->get();

        if (!$data["id"]) {
            $result["metaData"]["code"] = REST_Controller::HTTP_NO_CONTENT;
            $result["metaData"]["message"] = "Parameter tidak sesuai";
            $result["response"] = null;
            $this->response($result,   $result["metaData"]["code"]);
            break;
        }

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            $errorlist = $this->form_validation->error_array();
            $result["metaData"]["code"] = REST_Controller::HTTP_NOT_FOUND;
            $result["metaData"]["message"] = $errorlist;
            $result["response"] = null;
            $this->response($result,  $result["metaData"]["code"]);
        } else {
            $response = $this->registration->outPatientRegistrationList($data["id"]);
            $result["metaData"]["code"] = $response["metaData"]["code"];
            $result["metaData"]["message"] = $response["metaData"]["message"];
            $result["response"] = $response["response"];
            $this->response($result,  $result["metaData"]["code"]);
        }
        // $this->response($result,  $result["metaData"]["code"]);

    }

    private function sendEmail($email)
    {
        if ($email) {
            $config = [
                'protocol'    => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_user' => 'esensiana.com@gmail.com',
                'smtp_pass' => 'satujamlebihdekat14045',
                'smtp_port' =>     465,
                'mailtype'    => 'html',
                'charset'    => 'utf-8',
                'newline'    => "\r\n"
            ];

            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from('esensiana.com@gmail.com', 'RSUD AJIBARANG');
            $this->email->to($email);
            $data["title"] = "Tanda Bukti Pendaftaran Siap Ceria RSUD AJIBARANG";
            $this->email->subject($data["title"]);
            $this->email->message($this->load->view('tandabuktipendaftaran', $data, true));

            if ($this->email->send()) {
                return [
                    "message" => "Berhasil Kirim Imel",
                    "error" => null,
                    "status" => true
                ];
            } else {
                return [
                    "message" => "Gagal Kirim e-mail",
                    "status" => false,
                    "error" => $this->email->print_debugger()
                ];
            }
        }
    }

    public function validateNIK_get()
    {
        $nik = $this->get('nik');

        if ($nik) {
            $pasiendetails = $this->pasien->checkNIKexist($nik);
            $response["metaData"]["code"] = REST_Controller::HTTP_OK;
            $response["response"]["count"] = count($pasiendetails);
            if ($pasiendetails) {
                $response["metaData"]["message"] = "Ditemukan NIK";
                // $response["response"]["data"] = $pasiendetails;
            } else {
                $response["metaData"]["message"] = "Tidak ditemukan NIK";
            }

            $this->response($response,  $response["metaData"]["code"]);
        }
    }
}
