<?php

defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Auth extends BD_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->methods['users_get']['limit'] = 500;
        $this->methods['users_post']['limit'] = 100;
        $this->methods['users_delete']['limit'] = 50;
        $this->load->model('M_main', 'user');
        header('Content-Type: application/json');
        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Methods:  POST, GET');
    }

    public function login_post()
    {

        $u          = clean_data(htmlspecialchars($this->post('username'), true));
        $pwd        = clean_data(htmlspecialchars($this->input->post('password'), true));
        $q          = array('email' => $u);
        //
        $kunci = $this->config->item('thekey');
        $val = $this->user->get_user($q)->row();
        if ($this->user->get_user($q)->num_rows() == 0) {

            $result["metaData"]["code"] = REST_Controller::HTTP_NOT_FOUND;
            $result["metaData"]["message"] = "User Tidak DItemukan";
            $result["response"] = null;
            $this->response($result,  $result["metaData"]["code"]);
            //
        } else {
            $konfirmasi = $val->is_active;
            if ($konfirmasi > 0) {
                $passwor_in_db = $val->password;
                $cek = password_verify($pwd, $passwor_in_db);

                if ($cek == true) {
                    $token['id'] = $val->id;
                    $token['email'] = $u;
                    $token['firstname'] = $val->firstname;
                    $token['lastname'] = $val->lastname;
                    $token['nik'] = $val->nik;
                    $token['alamat'] = $val->alamat;
                    $token['nohp'] = $val->nohp;
                    $date = new DateTime();
                    $token['iat'] = $date->getTimestamp();
                    // $token['exp'] = $date->getTimestamp() + 60 * 60 * 5;
                    $token['exp'] =  strtotime("+1 week");
                    $output['token'] = JWT::encode($token, $kunci);
                    $result["metaData"]["code"] = REST_Controller::HTTP_OK;
                    $result["metaData"]["message"] = "OK";
                    $result["response"] = [
                        "token" =>  $output['token']
                    ];
                    $this->response($result,  $result["metaData"]["code"]);
                } else {
                    $result["metaData"]["code"] = REST_Controller::HTTP_UNAUTHORIZED;
                    $result["metaData"]["message"] = "PASSWORD SALAH";
                    $result["response"] = ["cek" => $cek];
                    $this->response($result,   $result["metaData"]["code"]);
                }
            } else {
                $result["metaData"]["code"] = REST_Controller::HTTP_UNAUTHORIZED;
                $result["metaData"]["message"] = "ANDA BELUM MELAKUKAN VALIDASI";
                $result["response"] = null;
                $this->response($result,  $result["metaData"]["code"]);
            }
        }
    }
}
