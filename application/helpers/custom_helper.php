<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('clean_data')) {

    function clean_data($string)
    {
        $ci =   get_instance();
        return $ci->security->xss_clean(trim(strip_tags(stripslashes($string))));
    }
}
