<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users_model  extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getLogin($usename, $password)
    {
        $userdetail = $this->db->get_where('users', [
            'username' => $usename,
            'password' => $password
        ]);

        return $userdetail;
    }
}
