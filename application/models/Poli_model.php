<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Poli_model  extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function getAllPoliklinik($kodePoli = null)
    {
        if ($kodePoli) {
            $query =  $this->db->get_where('reff_poli', ['KDPOLI' => $kodePoli]);
            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else {
                $message = ['message' => "Poli Tidak ditemukan"];
                return $message;
            }
        } else {
            $query =  $this->db->get('reff_poli')->result_array();
            return $query;
        }
    }
}
