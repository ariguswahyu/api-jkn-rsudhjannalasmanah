<?php if (!defined('BASEPATH')) exit('No direct script allowed');

class M_main extends CI_Model
{

	function get_user($q)
	{
		return $this->db->get_where('user', $q);
	}


	private function _hash_password($pass_user)
	{
		return password_hash($pass_user, PASSWORD_BCRYPT);
	}

	public function createUser($data)
	{


		$options = [
			'cost' => 10,
		];


		$payload["firstname"] = htmlspecialchars($data["firstname"], true);
		$payload["lastname"] = htmlspecialchars($data["lastname"], true);
		$payload["email"] = htmlspecialchars($data["email"], true);
		$payload["image"] = "default.png";
		$payload["password"] = $this->_hash_password($data["password"], $options);  //password_hash($data["password"], PASSWORD_DEFAULT);
		$payload["role_id"] = "95";
		$payload["is_active"] = "0";
		$payload["date_created"] = time();
		$this->db->insert('user', $payload);
		$result = $this->db->affected_rows();
		return $result;
	}

	// public function updateUser($data)
	// {
	// 	$this->db->set('firstname', addslashes($data["firstname"]));
	// 	$this->db->set('lastname', addslashes($data["lastname"]));
	// 	$this->db->where('id', htmlspecialchars($data["id"]));
	// 	$this->db->update('user');
	// 	return $this->db->affected_rows();
	// }

	public function updateUser($data)
	{
		// $bookingcode  = $this->_generateBookingCode(7);
		$this->db->set('alamat', htmlspecialchars($data["alamat"]));
		$this->db->set('nohp', htmlspecialchars($data["nohp"]));
		$this->db->where('id', htmlspecialchars($data["id"]));
		$this->db->update('user');
		$flag = $this->db->affected_rows();

		if ($flag == 1) {

			$result["metaData"]["code"] = REST_Controller::HTTP_OK;
			$result["metaData"]["message"] = "OK";
			$result["response"] =  ["update" => "1"];
			return  $result;
		} else {
			$result["metaData"]["code"] = REST_Controller::HTTP_NOT_MODIFIED;
			$result["metaData"]["message"] = "GAGAL";
			$result["response"] = null;
			return  $result;
		}
		// return $this->db->affected_rows();

	}


	public function getDetailByEmail($email)
	{
		$detail = $this->db->get_where('user', ['email' => $email])->row_array();

		return $detail;
	}



	public function getActiveUserByEmail($email)
	{
		$detail = $this->db->get_where('user', ['email' => $email, 'is_active' => 1])->row_array();

		return $detail;
	}

	function getDetailByEmail2($q)
	{
		$this->db->select('id,email');
		return $this->db->get_where('user', $q);
	}


	public function getDetailByID($id)
	{
		$detail = $this->db->get_where('user', ['id' => $id])->row_array();

		return $detail;
	}


	public function createToken($data)
	{

		$user_token =  [
			'email'            => $data["email"],
			'token'            => $data["token"],
			'date_created'    => time()
		];


		$this->db->insert('user_token', $user_token);
		$result = $this->db->affected_rows();
		return $result;
	}

	public function deleteToken($data)
	{
		$this->db->where('email', $data["email"]);
		$this->db->delete('user_token');
		return $this->db->affected_rows();
	}

	public function userActivating($data)
	{
		$this->db->set('is_active', 1);
		$this->db->where('email', $data["email"]);
		$this->db->update('user');
		return $this->db->affected_rows();
	}

	public function deleteUser($data)
	{
		$this->db->where('email', $data["email"]);
		$this->db->delete('user');
		return $this->db->affected_rows();
	}
}
